﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
namespace Kandooz.Sausage
{
    public class AIBehaviour_PickUpObject : AIBehaviour
    {
        public string itemName = "item";
        public Transform girlHand;
        private MovementHandler movement;
        private GirlHandState girlHandState;
        private InventoryManager inventory;
        public UnityEvent OnPickedUp;
        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);
            movement = myHandler.GetComponent<MovementHandler>();
            girlHandState = myHandler.GetComponent<GirlHandState>();
            inventory = myHandler.GetComponent<InventoryManager>();


            // Check errors b3den

            if (girlHandState.CheckHand(GirlHand.Right))
            {
                Debug.Log("Hand Not Free");
                Ended = true;
                return;
            }
            StartBehaviour();
        }
        protected override void StartBehaviour()
        {
            Started = true;
            movement.GoToDestination(this.transform.position,0.2f,OnReachedMe);
        }
        private void OnReachedMe()
        {
            Debug.Log("Picked");

            this.transform.parent = girlHand;
            this.transform.localPosition = Vector3.zero;
            this.transform.localEulerAngles = Vector3.zero;

            //inventory.AddSkill(itemName);
            //girlHandState.GiveItem(this.gameObject, GirlHand.Right);
            OnPickedUp.Invoke();
            EndBehaviour();

        }
        protected override void EndBehaviour()
        {
            Ended = true;
            Started = false;
        }
        public override void Interrupt()
        {
        }

        private void OnMouseDown()
        {
            FetchMe();
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                FetchMe();
            }
        }
        private void FetchMe()
        {
            AIHandler aIHandler = GameObject.FindObjectOfType<AIHandler>();
            aIHandler?.SetBehaviour(this);
        }
    }
}