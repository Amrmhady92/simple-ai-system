﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
namespace Kandooz.Sausage
{
    public class AIBehaviour_LightBraziers : AIBehaviour
    {
        public Transform artifact;
        public Transform artifactInteractPos;
        public List<FireBrazier> fireBraziers;
        public float flameDuration = 2;
        public UnityEvent onBraziersLit;
        private static FireTorch currentTorch;
        private bool hasTorch = false;
        private bool hasSpeed = false;
        private GameObject torchObject;
        private MovementHandler movementHandler;
        private GirlAnimationsEvents girlAnimations;

        private List<FireBrazier> nextBraziers;
        private FireBrazier currentBrazier;

        public static FireTorch CurrentTorch
        {
            get
            {
                return currentTorch;
            }

            set
            {
                currentTorch = value;
            }
        }

        public void SelfInit(FireBrazier brazier)
        {
            AIHandler handler = GameObject.FindObjectOfType<AIHandler>();
            if (handler != null) handler.SetBehaviour(this);
            currentBrazier = brazier;
        }
        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);
            Interruptible = false;
            if (fireBraziers == null && fireBraziers.Count != 3)
            {
                Debug.Log("No braziers Referenced, Nothing Happens");
                Ended = true;
                return;
            }
            
            nextBraziers = new List<FireBrazier>(fireBraziers); // The Next Two Braziers
            if (nextBraziers.Contains(currentBrazier)) nextBraziers.Remove(currentBrazier);

            //Testing
            string dbg = "";
            for (int i = 0; i < nextBraziers.Count; i++)
            {
                dbg += nextBraziers[i].name + "\n";
            }

            // End testing

            InventoryManager inventory = myHandler.GetComponent<InventoryManager>(); // Get The inventory from the girl(handler)
            if (inventory != null)
            {
                if (inventory.SearchSkill("LitTorch"))
                {
                    Debug.Log("Has Torch");
                    if (CurrentTorch == null) // But is it really there?
                    {
                        Debug.Log("But No currentTorch , although Has Torch");
                        Ended = true;
                        return;
                    }
                    CurrentTorch.DisableDropping();
                    // Fire Up the Thing
                    hasTorch = true;
                    
                }
                else
                {
                    Debug.Log("Doesnt Have Torch");
                    // SHRUG
                    hasTorch = false;
                }

                if (inventory.SearchSkill("Speed"))
                {
                    Debug.Log("Has Speed");
                    // Fire Up the Thing Fast
                    hasSpeed = true;
                }
                else
                {
                    Debug.Log("Doesnt Have Speed");
                    // Fire Up the Thing Slowly
                    hasSpeed = false;
                }
            }
            else
            {
                Debug.Log("No Inventory on Character , Nothing Happens");
                Ended = true;
                return;
            }

            movementHandler = myHandler.GetComponent<MovementHandler>();
            if (movementHandler == null)
            {
                Debug.Log("No MovementHandler on Character , Nothing Happens");
                Ended = true;
                return;
            }

            // START AFTER EVERY THING IS IN PLACE
            StartBehaviour();
            girlAnimations = myHandler.GetComponent<GirlAnimationsEvents>();
        }






        protected override void StartBehaviour()
        {
            Started = true;
            //movementHandler.GoToDestination(currentBrazier.transform.position, 2f, OnReachedEvent: OnReachBrazier);
            movementHandler.GoToDestination(currentBrazier.StoppingPositions.GetRandomValue().position, 1f, OnReachedEvent: OnReachBrazier);
            Debug.Log("Start Moving");

        }

        private void OnReachBrazier()
        {
            

            if (hasTorch)
            {
                Debug.Log("Going again");
                OnReachedAgain();
                //movementHandler.GoToDestination(currentBrazier.transform.position, 2f, OnReachedEvent: OnReachedAgain);
            }
            else
            {
                Debug.Log("Not Torch, Shrug");
                StartCoroutine(WaitAndExcute(2, EndBehaviour));
            }
        }

        private void OnReachedAgain()
        {
            Debug.Log("Firing UP");
            // Now Should Play Animation of Turning on fire, after finish next should happen
            // On Finsh animaion do this next (without the wait)
            StartCoroutine(WaitAndExcute(1, OnFireUpBrazier));
        }

        private void OnFireUpBrazier()
        {
            currentBrazier.FireUpBrazier(hasSpeed); // Fire up the brazier

            //For Now Wait then go to next if no Next restore the torch.
            if (nextBraziers.Count > 0)
            {
                currentBrazier = nextBraziers.GetRandomValue();
                nextBraziers.Remove(currentBrazier);
                StartCoroutine(WaitAndExcute(1, () =>
                {
                    movementHandler.GoToDestination(currentBrazier.StoppingPositions.GetRandomValue().position, 1f, OnReachBrazier);
                }));
            }
            else // No More Braziers
            {
                DropTorch();

                // Check all the Braziers if Lit
                bool allLit = true;
                for (int i = 0; i < fireBraziers.Count; i++)
                {
                    if (!fireBraziers[i].onFire)
                    {
                        allLit = false;
                        break;
                    }
                }

                if (allLit) // If all of them are lit
                {
                    // dont make them turn off again 5las
                    for (int i = 0; i < fireBraziers.Count; i++)
                    {
                        fireBraziers[i].canPutOff = false;
                    }

                    Debug.Log("<color=green>ALL LIT ALL HAIL THE ARTEFACT</color> ");
                    onBraziersLit?.Invoke();
                    StartCoroutine(WaitAndExcute(2, Wonder));

                } // if Not
                else
                {
                    Debug.Log("<color=red>NOTTING HAPPENS</color>");
                    WaitToEnd();
                }
                
                
            }
        }

        private void Wonder()
        {
            girlAnimations.Wonder(artifact, artifactInteractPos);
            girlAnimations.OnAnimationEnd += WaitToEnd;
        }

        private void WaitToEnd()
        {
            StartCoroutine(WaitAndExcute(4, EndBehaviour));
        }

        protected override void EndBehaviour()
        {
            Ended = true;
            Started = false;
        }
        public override void Interrupt()
        {

        }
        public void AddTorchToInventory(GameObject torch)
        {
            if(torch == null)
            {
                Debug.Log("No Game Object referenced");
                return;
            }
            
            InventoryManager inventory = GameObject.FindObjectOfType<InventoryManager>(); // Get The inventory from the scene
            GirlHandState girlHandState = inventory.gameObject.GetComponent<GirlHandState>();
            if(girlHandState == null) girlHandState = GameObject.FindObjectOfType<GirlHandState>();
            if(girlHandState == null)
            {
                Debug.Log("No Hand State Found Returning");
                return;
            }

            if (girlHandState.CheckHand(GirlHand.Right))
            {
                Debug.Log("Hand is not free");
                return;
            }
            else
            {
                girlHandState.RegisterItem(torch,GirlHand.Right);
                torchObject = torch;
            }
            

            if (inventory != null)
            {

                if (inventory.AddSkill("Torch"))
                {
                    Debug.Log("Torch Add to Inventory");
                    //SelfInit();
                }
                else
                {
                    Debug.Log("Couldnt Add to Inventory");
                }

            }
            else
            {
                Debug.Log("No Inventory on Character , Nothing Happens");
            }
        }
        public void AddSpeedToInventory()
        {
            InventoryManager inventory = GameObject.FindObjectOfType<InventoryManager>(); // Get The inventory from the scene
            inventory?.AddSkill("Speed");
            Debug.Log("Speed Add");
        }
        private void DropTorch()
        {
            CurrentTorch.DropTorch();
            CurrentTorch = null;
            //InventoryManager inventory = GameObject.FindObjectOfType<InventoryManager>(); // Get The inventory from the scene
            //GirlHandState girlHandState = inventory.gameObject.GetComponent<GirlHandState>();
            //if (girlHandState == null) girlHandState = GameObject.FindObjectOfType<GirlHandState>();
            //if (girlHandState == null)
            //{
            //    Debug.Log("No Hand State Found Returning");
            //    return;
            //}

            //girlHandState.UnRegisterItem(GirlHand.Right);

            //if (inventory != null)
            //{
            //    inventory.RemoveSkill("Torch");
            //}

            //torchObject.SetActive(false);
            //torchObject.transform.parent = null;

        }
        private IEnumerator WaitAndExcute(float time, System.Action callback)
        {
            yield return new WaitForSeconds(time);
            callback.Invoke();
        }

        public void SetFireTorch(FireTorch torch)
        {
            CurrentTorch = torch;
        }
    }
}