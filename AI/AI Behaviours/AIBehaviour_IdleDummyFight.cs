﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Kandooz.Sausage
{
    public class AIBehaviour_IdleDummyFight : AIBehaviour
    {
        public Transform interactionPosition;
        public int howManySwings;
        public bool canInterrupt;

        private MovementHandler girlMovementHandler;
        private GirlAnimationsEvents girlAnimations;
        private GirlHandState handstate;
        private InventoryManager girlInventory;
        private bool fighting;
        private int counter;


        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);
            StartBehaviour();
        }


        protected override void StartBehaviour()
        {
            girlMovementHandler = myHandler.GetComponent<MovementHandler>();
            girlAnimations = myHandler.GetComponent<GirlAnimationsEvents>();
            handstate = myHandler.GetComponent<GirlHandState>();
            girlInventory = myHandler.GetComponent<InventoryManager>();

            if (!handstate.CheckHand(GirlHand.Right) && girlInventory.SearchSkill("Sword"))
            {

                Started = true;
                Interruptible = true;
                girlMovementHandler.GoToDestination(interactionPosition.position, 0.08f, turnToPosition: transform, OnReachedEvent: Fight);
            }
            else
            {
                EndBehaviour();
            }
        }

        private void Fight()
        {
            fighting = true;
            girlAnimations.DrawSword();
            girlAnimations.OnAnimationEnd += SwordDrawed;
        }

        public void SwordDrawed()
        {
            girlAnimations.OnAnimationEnd -= SwordDrawed;
            girlAnimations.FightDummy(true);
            girlAnimations.OnAnimationEnd += OnSwingEnd;
        }

        private void OnSwingEnd()
        {
            counter++;
            if(counter >= howManySwings)
            {
                girlAnimations.OnAnimationEnd -= OnSwingEnd;
                girlAnimations.FightDummy(false);
                girlAnimations.SheathSword();
                girlAnimations.OnAnimationEnd += SwordSheathed;
            }
        }


        private void SwordSheathed()
        {
            girlAnimations.OnAnimationEnd -= SwordSheathed;
            girlAnimations.SwordSheathed();
            EndBehaviour();
        }

        protected override void EndBehaviour()
        {
            fighting = false;
            counter = 0;
            Ended = true;
            Started = false;
        }

        public override void Interrupt()
        {
            if (fighting)
            {
                counter = howManySwings;
            }
            else
                EndBehaviour();

        }

        void Start()
        {
        }
    }
}