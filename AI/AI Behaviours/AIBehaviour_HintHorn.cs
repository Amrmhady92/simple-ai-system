﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Kandooz.Sausage
{
    public class AIBehaviour_HintHorn : AIBehaviour
    {
        public GameObject horn;
        public float pointingDuration = 4;
        private NPCLookAt npcLookAt;
        private Animator animator;
        private HornBlowing playerHornBlowing;


        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);

            npcLookAt = ServiceLocator.GetService<NPCLookAt>(false);
            animator = myHandler.GetComponent<Animator>();
            playerHornBlowing = ServiceLocator.GetService<HornBlowing>(false);

            Interruptible = true;
            if (npcLookAt == null || playerHornBlowing == null)
            {
                Debug.LogError("NpcLookat or playerHornBlowing Not found in Scene , Behaviour not started");
                Ended = true;
                return;
            }
            StartBehaviour();
        }

        protected override void StartBehaviour()
        {
            Started = true;
            Debug.Log("GIRL HINTING START");

            animator.SetBool("GirlHornHint", true);
            playerHornBlowing.HornBlowStart.AddListener(EndBehaviour);
            StartCoroutine(WaitThenExcute(pointingDuration, EndBehaviour));
        }

        public void InitiatePointing() //called by animation event on HornHint animation
        {
            npcLookAt.PointHand(horn, AvatarIKGoal.RightHand);
        }

        protected override void EndBehaviour()
        {
            StopAllCoroutines();
            playerHornBlowing.HornBlowStart.RemoveListener(EndBehaviour);
            animator.SetBool("GirlHornHint", false);
            npcLookAt.PointHand(null, AvatarIKGoal.RightHand);

            Ended = true;
            Started = false;
        }

        public override void Interrupt()
        {
            EndBehaviour();
        }

        IEnumerator WaitThenExcute(float time, Action callback)
        {
            yield return new WaitForSeconds(time);
            callback.Invoke();
        }
    }
}
