﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Kandooz.Sausage
{

    public class AIBehaviour_DrinkPotion : AIBehaviour
    {
        public GameObject potionObject; // Should be this
        public AlchemyOutput potionBehaviour;

        private GirlAnimationsEvents girlAnimations;

        private bool drinking = false;

        public void SelfInit()
        {
            AIHandler handler = GameObject.FindObjectOfType<AIHandler>();
            if (potionBehaviour == null) potionBehaviour = this.GetComponent<AlchemyOutput>();
            IsVeryHighPriority = true;
            if (handler != null && potionBehaviour != null) handler.SetBehaviour(this); 
        }
        public override void Init(AIHandler aIHandler)
        {

            base.Init(aIHandler);

            girlAnimations = myHandler.GetComponent<GirlAnimationsEvents>();

            if (potionObject == null)
            {
                Debug.Log("Potion Object Not Referenced");
                potionObject = this.gameObject;
            }
            Interruptible = false;
            if (potionBehaviour != null)
            {
                StartBehaviour();
            }
            else
            {
                Debug.Log("No Potion Behaviour on potion, ending");
                EndBehaviour();
            }
        }

        protected override void StartBehaviour()
        {
            Interruptible = false;
            Started = true;
            
            StartCoroutine(Wait(1,PlayDrink));
        }

        protected override void EndBehaviour()
        {

            Ended = true;
            Started = false;
            // Drop the Potion
            this.transform.parent = null;
            this.GetComponent<Collider>().isTrigger = false;
            this.GetComponent<Rigidbody>().isKinematic = false;

        }

        private void PlayDrink()
        {
            // Play Animation Drink, On End 
           // Debug.Log("Drinking");
            girlAnimations.Drink();
            girlAnimations.OnAnimationEnd += FinishDrink;
            //StartCoroutine(Wait(3,FinishDrink));

        }
        private void FinishDrink()
        {
         //   Debug.Log("Finished Drinking");
            girlAnimations.OnAnimationEnd -= FinishDrink;

            // CHANGE TO POTION
            //myHandler.SetBehaviour(potionBehaviour, 100);
            potionBehaviour?.OnPotionDrank.Invoke();

            StartCoroutine(Wait(1, EndBehaviour));
        }

        public override void Interrupt()
        {
            if (!drinking)
            {
                StopAllCoroutines();
                StartCoroutine(Wait(1, EndBehaviour));
            }           
        }

        private IEnumerator Wait(float waitTime, System.Action callBack)
        {
            yield return new WaitForSeconds(waitTime);
            callBack?.Invoke();
        }


    }
}