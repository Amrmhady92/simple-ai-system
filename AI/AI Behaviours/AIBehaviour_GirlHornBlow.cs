﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kandooz.Sausage
{
    public class AIBehaviour_GirlHornBlow : AIBehaviour
    {

        private Animator animator;

        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);

            animator = myHandler.GetComponent<Animator>();
           
            StartBehaviour();
        }

        protected override void StartBehaviour()
        {
            Interruptible = false;
            Started = true;

            animator.SetBool("GirlHornBlow", true);
        }

        public void GirlFinishedHowling() //called by animation event "Horn Blow"
        {
            if (!Started) return;

            animator.SetBool("GirlHornBlow", false);
            EndBehaviour();
            ObjectShake[] objectsToShake;
            objectsToShake = FindObjectsOfType<ObjectShake>();

            for (int i = 0; i < objectsToShake.Length; i++)
            {
                objectsToShake[i].Shake(5);
            }
        }

        protected override void EndBehaviour()
        {
            Ended = true;
            Started = false;
        }

        public override void Interrupt()
        {

        }
    }
}