﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Kandooz.Sausage
{
    public class AIBehaviour_GrowPlant : AIBehaviour
    {
        private MovementHandler movementHandler;
        private GameObject plantPosition;
        private ChilliPlant plant;
        private GameObject seedsObejct;
        private bool hasSeeds = false;
        private bool reached = false;

        public void AddSeedsToInventory(GameObject seedsObejct)
        {
            InventoryManager inventory = GameObject.FindObjectOfType<InventoryManager>(); // Get The inventory from the scene
            if (inventory != null)
            {
                if (inventory.AddSkill("Seeds"))
                {
                    Debug.Log("Seeds Add to Inventory");
                    SelfInit();
                }
                else
                {
                    Debug.Log("Couldnt Add Inventory");
                }
            }
            else
            {
                Debug.Log("No Inventory on Character , Nothing Happens");
            }
            this.seedsObejct = seedsObejct;
        }

        public void SelfInit()
        {
            var handler = GameObject.FindObjectOfType<AIHandler>();
            IsVeryHighPriority = true;
            if (handler != null) handler.SetBehaviour(this);
            else Debug.Log("AIHandler not found in scene");
        }

        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);
            Interruptible = true; 

            InventoryManager inventory = myHandler.GetComponent<InventoryManager>(); // Get The inventory from the girl(handler0
            if (inventory != null)
            {
                if (inventory.SearchSkill("Seeds"))
                {
                    Debug.Log("Has Seeds and Growing");
                    // GROW THE PLANT
                    hasSeeds = true;
                }
                else
                {
                    Debug.Log("Doesnt Have Seeds");
                    // SHRUG
                    hasSeeds = false;
                }
            }
            else
            {
                Debug.Log("No Inventory on Character , Nothing Happens");
                Ended = true;
                return;
            }

            // Go to Plant // object is waypoint, should have the Tag "PlantPosition" on it
            plantPosition = GameObject.FindGameObjectWithTag("PlantPosition");
            if (plantPosition == null)
            {
                Debug.LogError("No Plant Found, Nothing Happens");
                Ended = true;
                return;
            }

            movementHandler = myHandler.GetComponent<MovementHandler>();  // Make Sure the movement handler is available
            if (movementHandler == null) movementHandler = GameObject.FindObjectOfType<MovementHandler>();
            if (movementHandler == null)
                {
                Debug.LogError("No Movement Handler, Nothing Happens");
                Ended = true;
                return;
            }

            plant = GameObject.FindObjectOfType<ChilliPlant>();
            if (plant == null)
            {
                Debug.LogError("No Plant found, Nothing Happens");
                Ended = true;
                return;
            }

            //When everything is fine Start;;
            StartBehaviour();
        }

        protected override void StartBehaviour()
        {
            Started = true;
            reached = false;
            movementHandler.GoToDestination(plantPosition.transform.position, 0.2f, OnReachedEvent: PlantReached); // Move to the plant pos, max reach dist  = 0.2f , Invoke Plant Reached on arrival
        }

        public override void Interrupt()
        {
            if (!Interruptible) return;
            if (!reached)
            {
                if(movementHandler != null)
                {
                    movementHandler.Stop();
                    EndBehaviour();
                }
            } 
        }

        protected override void EndBehaviour()
        {
            Started = false;
            Ended = true;
        }

        private void PlantReached()
        {
            reached = true; // Mark as reached

            Interruptible = false; // Cant be interrupted now

            // Turn To Plant

            // Check if has seeds
            if (hasSeeds)
            {
                Debug.Log("Planting");
                plant.StartGrowing();
                if(seedsObejct!= null)
                {
                    LeanTween.scale(seedsObejct, Vector3.zero, 1).setOnComplete(()=> { seedsObejct.SetActive(false); });
                }
            }
            else
            {
                Debug.Log("Shrugging");
                //Play Animation Here
            }

            Invoke("EndBehaviour", 2f);
        }
    }
}

