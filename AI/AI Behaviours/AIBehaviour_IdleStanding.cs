﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kandooz.Sausage
{
    public class AIBehaviour_IdleStanding : AIBehaviour
    {

        public override void Init(AIHandler aIHandler)
        {

            base.Init(aIHandler);
            

            StartBehaviour();
        }

        protected override void StartBehaviour()
        {
            Started = true;
            Interruptible = true;
            
            StartCoroutine(Wait());

        }

        protected override void EndBehaviour()
        {
            
            Ended = true;
            Started = false;

        }

        private IEnumerator Wait()
        {
            yield return new WaitForSeconds(Random.Range(4,6));
           
            yield return new WaitForSeconds(1);
            EndBehaviour();
        }


        public override void Interrupt()
        {
            StopAllCoroutines();
            StartCoroutine(Wait(1, EndBehaviour));
        }

        private IEnumerator Wait(float waitTime, System.Action callBack)
        {
            yield return new WaitForSeconds(waitTime);
            callBack.Invoke();
        }


    }
}