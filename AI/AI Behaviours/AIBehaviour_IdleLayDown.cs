﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kandooz.Sausage
{
    public class AIBehaviour_IdleLayDown : AIBehaviour
    {
        public List<Transform> laydDownpositions;
        public int napDuration;
        public bool canInterrupt; //used bell to wake her up

        private MovementHandler girlMovementHandler;
        private GirlAnimationsEvents girlAnimations;
        private GirlHandState handstate;
        private bool sleeping;
        private bool yawning;

        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);
            if (handstate == null) handstate = myHandler.GetComponent<GirlHandState>();
            if (handstate == null) Debug.Log("No Hand State");
            else if (handstate.CheckHand(GirlHand.Right))
            {
                EndBehaviour();
                return;
            }
            StartBehaviour();
        }

        protected override void StartBehaviour()
        {
            girlMovementHandler = myHandler.GetComponent<MovementHandler>();
            girlAnimations = myHandler.GetComponent<GirlAnimationsEvents>();
            handstate = myHandler.GetComponent<GirlHandState>();

            if (!handstate.CheckHand(GirlHand.Right))
            {
                Started = true;
                Interruptible = true;
                FindPlace();
            }
            else
                EndBehaviour();
        }


        private void FindPlace()
        {
            int rndNum = Random.Range(0, laydDownpositions.Count);

            girlMovementHandler.GoToDestination(laydDownpositions[rndNum].position, 0.08f, turnToPosition: transform, OnReachedEvent: Yawn);

        }

        private void Yawn()
        {
            Interruptible = true;
            yawning = true;
            girlAnimations.Yawn();
            girlAnimations.OnAnimationEnd += LayDown;
        }

        private void LayDown()
        {
            //Interruptible = true;
            yawning = false;
            girlAnimations.OnAnimationEnd -= LayDown;
            girlAnimations.LayDown(true);
            sleeping = true;
            StartCoroutine(GetUp());
        }

        IEnumerator GetUp()
        {
            yield return new WaitForSeconds(napDuration);

            girlAnimations.LayDown(false);
            EndBehaviour();
        }

        private void Revive()
        {
            girlAnimations.Revive();
            EndBehaviour();
        }

        public override void Interrupt()
        {
            if (sleeping)
            {
                if (canInterrupt) // used bell to wake her up
                {
                    StopCoroutine(GetUp());
                    girlAnimations.LayDown(false);
                    Revive();
                }
            }
            else
            {
                if(!yawning)
                    EndBehaviour();
                else //yawning
                {
                    girlAnimations.OnAnimationEnd -= LayDown;
                    EndBehaviour();
                }
            }
        }

        protected override void EndBehaviour()
        {
            yawning = false;
            sleeping = false;
            Ended = true;
            Started = false;
        }

        private void Start()
        {
        }
    }
}
