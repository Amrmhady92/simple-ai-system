﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using Kandooz.KVR;

namespace Kandooz.Sausage
{

    public class AIBehaviour_UnlockCabinet : AIBehaviour
    {
        public Transform interactionPosition;
        public UnityEvent onCabinetUnlocked;
        public InteractableFarInteraction interactable;

        public GameObject cabinetDoorLeft;
        public GameObject cabinetDoorRight;

        private InventoryManager girlInventory;
        private MovementHandler girlMovement;
        private GirlAnimationsEvents girlAnimationsEvents;
        private GirlHandState handstate;

        private bool used;
        private bool hasSword = false;

        private void OnMouseDown()
        {
            FetchMe();
        }


        public void FetchMe()
        {
            if (used) //already unlocked
            {
                Debug.Log("Already used");
                return;
            }

            FindObjectOfType<AIHandler>().SetBehaviour(this);
            if (interactable != null)
            {
                interactable.OnUnHover();
                interactable.Interactable = false;
                
            }
        }

        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);

            Interruptible = false;

            girlInventory = myHandler.GetComponent<InventoryManager>();
            girlMovement = myHandler.GetComponent<MovementHandler>();
            girlAnimationsEvents = myHandler.GetComponent<GirlAnimationsEvents>();
            handstate = myHandler.GetComponent<GirlHandState>();

            if (girlInventory.SearchSkill("Sword"))
                hasSword = true;

            if(handstate.CheckHand(GirlHand.Right))
            {
                girlAnimationsEvents.Shrug();
                girlAnimationsEvents.OnAnimationEnd += Shrugged;
            }
            else
                StartBehaviour();
        }


        protected override void StartBehaviour()
        {
            Interruptible = false;
            Started = true;

            girlMovement.GoToDestination(interactionPosition.position, 1f, turnToPosition: transform, OnReachedEvent: OnReachCabinet, OnFailedToMove: EndBehaviour);
        }


        private void OnReachCabinet()
        {
            if(hasSword)
            {
                //unlock with sword
                Debug.Log("Unsheath sword");
                Debug.Log("has sword, unlock cabinet");
                girlAnimationsEvents.DrawSword();
                girlAnimationsEvents.OnAnimationEnd += SwordDrawed;
            }
            else
            {
                //shrug
                Debug.Log("No Sword, shrug");
                girlAnimationsEvents.Shrug();
                girlAnimationsEvents.OnAnimationEnd += Shrugged;
            }
        }

        public void Shrugged()
        {
            girlAnimationsEvents.OnAnimationEnd -= Shrugged;
            EndBehaviour();
        }

        public void SwordDrawed()
        {
            Debug.Log("Drawed");
            girlAnimationsEvents.OnAnimationEnd -= SwordDrawed;
            //girlAnimationsEvents.SwordDrawed();
            girlAnimationsEvents.UnlockCabinet();
            girlAnimationsEvents.OnAnimationEnd += OnCabinetUnlocked;
        }

        private void OnCabinetUnlocked()
        {

            used = true;

            onCabinetUnlocked?.Invoke();
            

            girlAnimationsEvents.OnAnimationEnd -= OnCabinetUnlocked;
            girlAnimationsEvents.SheathSword();
            girlAnimationsEvents.OnAnimationEnd += SwordSheathed;
        }

        public void OpenDoors()
        {
            LeanTween.value(cabinetDoorLeft.transform.localEulerAngles.y, 140, 1).setOnUpdate((float v) =>
            {
                cabinetDoorLeft.transform.localEulerAngles = new Vector3(0, v, 0);
            });
            LeanTween.value(cabinetDoorRight.transform.localEulerAngles.y, -140, 1).setOnUpdate((float v) =>
            {
                cabinetDoorRight.transform.localEulerAngles = new Vector3(0, v, 0);
            });
            Debug.Log("Doors Opening");
            //if (cabinetDoorLeft != null) LeanTween.rotateY(cabinetDoorLeft, 120, 1);
            //if (cabinetDoorRight != null) LeanTween.rotateY(cabinetDoorRight, -120, 1);
        }

        private void SwordSheathed()
        {
            girlAnimationsEvents.OnAnimationEnd -= SwordSheathed;
            girlAnimationsEvents.SwordSheathed();
            EndBehaviour();
        }

        protected override void EndBehaviour()
        {
            Ended = true;
            Started = false;
            if (interactable != null) interactable.Interactable = !used;
        }
        

        public override void Interrupt()
        {

        }

      
    }
}