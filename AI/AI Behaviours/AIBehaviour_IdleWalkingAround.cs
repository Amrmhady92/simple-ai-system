﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

namespace Kandooz.Sausage
{
    public class AIBehaviour_IdleWalkingAround : AIBehaviour
    {
        public bool useAIController = true;
        public float radius = 5f;
        public float minWalkDuration = 5f;
        public float maxWalkDuration = 10f;
        private UnityStandardAssets.Characters.ThirdPerson.AICharacterControl navController;
        private NavMeshAgent navAgent;
        private Transform target;
        private Vector3 position;
        private MovementHandler movementHandler;

        public override void Init(AIHandler aIHandler)
        {
            Debug.Log("Walking Around Called");
            base.Init(aIHandler);
            Started = true;
            StartBehaviour();
        }

        protected override void StartBehaviour()
        {
            interruptible = true;
            navAgent = myHandler.GetComponent<NavMeshAgent>();
            navController = myHandler.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>();
            movementHandler = myHandler.GetComponent<MovementHandler>();
            position = Utilities.RandomNavSphere(this.transform.position, radius, 2f, useIterations: true, iterationsCount: 10);


            if (useAIController && target == null && movementHandler == null)
            {
                target = new GameObject("WalkAroundTarget").transform;
                target.transform.position = position;
            }
            


            if (movementHandler == null)
            {
                if (navController != null && useAIController)
                {
                    navController.SetTarget(target);
                }

                else if (navAgent != null)
                {
                    navAgent.destination = Utilities.RandomNavSphere(this.transform.position, radius, useIterations: true, iterationsCount: 5);
                    navAgent.isStopped = false;
                }
            }
            else
            {

                movementHandler.GoToDestination(position, 0.1f/*, OnReachedEvents: new List<System.Action>() { () => { Debug.Log("REACHED FROM IDLE WALK"); } }*/);
            }
            
            

            
            StartCoroutine(Wait());

        }

        protected override void EndBehaviour()
        {
            ended = true;
            started = false;
            //Destroy(target.gameObject);
        }

        private IEnumerator Wait()
        {
            Debug.Log("Wait Started");
            yield return new WaitForSeconds(Random.Range(minWalkDuration, maxWalkDuration));
            Stop();
            yield return new WaitForSeconds(1);
            EndBehaviour();
        }
        private IEnumerator WaitQuickEnd()
        {
            Debug.Log("Interrupt Wait Started");
            Stop();
            yield return new WaitForSeconds(1);
            EndBehaviour();
        }
        public override void Interrupt()
        {
            StopAllCoroutines();
            StartCoroutine(WaitQuickEnd());
        }

        private void Stop()
        {
            //if (meshRenderer != null)
            //{
            //    meshRenderer.material.color = defaultMeshColor;
            //}
            if (movementHandler == null)
            {
                if (navAgent != null && !useAIController)
                {
                    navAgent.isStopped = true;
                }
                else if (navController != null && useAIController)
                {
                    navController.SetTarget(navController.transform);
                }
            }
            else
            {
                movementHandler.Stop();
            }

           
        }
    }
}