﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kandooz.Sausage
{
    public class AIBehaviour_LightTorch : AIBehaviour
    {
        public Transform comePosition;
        public Transform lookPosition;
        public float minStopDistance = 0.5f;
        public static FireTorch currentFireTorch;
        
        MovementHandler movement;
        GirlAnimationsEvents girlAnimationsEvents;
        public void SelfInit()
        {
            GameObject.FindObjectOfType<AIHandler>()?.SetBehaviour(this);
        }
        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);
            bool initfail = false;
            string debugInitFail = "";

            movement = myHandler.GetComponent<MovementHandler>();
            girlAnimationsEvents = myHandler.GetComponent<GirlAnimationsEvents>();

            if (  movement   == null) { initfail = true; debugInitFail += "movementHandler not found on Handler \n"; }
            if (comePosition == null) { initfail = true; debugInitFail += "comePosition not Referenced \n"; }
            if (lookPosition == null) { initfail = true; debugInitFail += "lookPosition not Referenced \n"; }
            if (girlAnimationsEvents == null) { initfail = true; debugInitFail += "girlAnimationsEvents not found on Handler \n"; }

            if (initfail)
            {
                Debug.Log(debugInitFail);
                EndBehaviour();
                return;
            }

            Interruptible = false;
            StartBehaviour();

        }

        protected override void StartBehaviour()
        {
            Started = true;
            movement.GoToDestination(comePosition.position, minStopDistance, OnReachedFirePlace, turnToPosition: lookPosition, OnFailedToMove: OnFailedToReachFirePlace);
        }
        protected override void EndBehaviour()
        {
            Debug.Log("AIBehaviour_LightTorch :: EndBehaviour");
            Started = false;
            Ended = true;
        }
        public override void Interrupt()
        {
            //if (Interruptible)
            //{
            //    Debug.Log("Firing Interrupted");
            //    movement.GoToDestination(myHandler.transform.position, 0.5f, () => { EndBehaviour(); },turnToObjectOnReach:false);
            //}
        }

        private void OnReachedFirePlace()
        {


            Interruptible = false;
            if(currentFireTorch != null) // Has Torch
            {
                Debug.Log("Firing Torch");
                currentFireTorch.LightTorch();
                Invoke("EndBehaviour", 1);
            }
            else
            {
                Debug.Log("Doesnt Have a Torch, Shrugs");

                girlAnimationsEvents.OnAnimationEnd += Shrugged;
                girlAnimationsEvents.Shrug();
            }
        }

        private void Shrugged()
        {
            Debug.Log("Shrugged");
            girlAnimationsEvents.OnAnimationEnd -= Shrugged;
            Invoke("EndBehaviour", 1);
        }
        private void OnFailedToReachFirePlace()
        {
            Debug.Log("Failed to Reach Fireplace");
            girlAnimationsEvents.OnAnimationEnd += Shrugged;
            girlAnimationsEvents.Shrug();
        }

        private void OnMouseDown()
        {
            SelfInit();
        }
    }
}