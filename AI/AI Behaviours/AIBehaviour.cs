﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Kandooz.Sausage
{

    public abstract class AIBehaviour : MonoBehaviour
    {
        protected bool interruptible;
        protected bool ended = false;
        protected bool started = false;
        protected int priority = 0;
        [SerializeField] protected bool isVeryHighPriority = false;

        protected AIHandler myHandler;



        public int Priority
        {
            get
            {
                return priority;
            }
            set
            {
                priority = Mathf.Max(0, value);
            }
        }
        public bool Started
        {
            get
            {
                return started;
            }

            protected set
            {
                started = value;
            }

        }
        public bool Ended
        {
            get
            {
                return ended;
            }
            protected set { ended = value; }
        }
        public bool Interruptible
        {
            get
            {
                return interruptible;
            }

            protected set { interruptible = value; }

        }
        public bool IsVeryHighPriority
        {
            get
            {
                return isVeryHighPriority;
            }

            protected set
            {
                isVeryHighPriority = value;
            }
        }

        /// <summary>
        /// the Init funcition must be called to start the 
        /// </summary>
        /// <param name="aIHandler">The handler of this AI behaviour, the handler should always call this function and no one else, should send itself</param>
        public virtual void Init(AIHandler aIHandler)
        {
            myHandler = aIHandler;
            ended = false;
        }



        /// <summary>
        /// Should be called by the Handler, if the current state of the behaviour is interruptible,
        /// then this should be called to handle the ending of the behaviour 
        /// for handing over to the next behaviour
        /// </summary>
        public abstract void Interrupt();


        protected abstract void StartBehaviour();
        protected abstract void EndBehaviour();


    }
}