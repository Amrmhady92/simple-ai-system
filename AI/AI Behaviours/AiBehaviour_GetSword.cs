﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using UnityEngine.Events;

namespace Kandooz.Sausage
{
    public class AiBehaviour_GetSword : AIBehaviour
    {
        public FullBodyBipedEffector[] struggleEffectors;
        public FullBodyBipedEffector[] pullSwordEffectors;
        public InteractionObject struggleInteraction;
        public InteractionObject pullSwordInteraction;
        public GameObject rock;
        public GameObject magnetStone;
        public Transform interactionPosition;
        public UnityEvent onSwordPulled;

        private GirlAnimationsEvents girlAnimationsEvents;
        private InventoryManager girlInventory;
        private InteractionSystem girlInteractionSystem;
        private GirlIK girlIK;
        private MovementHandler girlMovementHandler;
        private GirlHandState handstate;

        private bool hasSword;
        private bool hasStrength;


        private void Start()
        {
            

        }


        private void OnMouseDown()
        {
            FetchMe();
        }

        public void FetchMe()
        {
            var handler = GameObject.FindObjectOfType<AIHandler>();
            girlInventory = handler.GetComponent<InventoryManager>();
            handstate = handler.GetComponent<GirlHandState>();

            //check her skills
            if (girlInventory != null)
            {
                hasSword = girlInventory.SearchSkill("Sword");
                hasStrength = girlInventory.SearchSkill("Strength");
            }
            else
            {
                Debug.Log("No Inventory on girl");
                return;
            }

            if (hasSword)
            {
                Debug.Log("already has sword");
                return;
            }

            if (handler != null)
            {
                handler.SetBehaviour(this);
            }
        }

        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);

            if (girlInventory != null) hasStrength = girlInventory.SearchSkill("Strength");

            girlAnimationsEvents = myHandler.GetComponent<GirlAnimationsEvents>();
            girlInteractionSystem = myHandler.GetComponent<InteractionSystem>();
            girlIK = myHandler.GetComponent<GirlIK>();
            girlMovementHandler = myHandler.GetComponent<MovementHandler>();

            if (handstate.CheckHand(GirlHand.Right))
            {
                girlAnimationsEvents.Shrug();
                girlAnimationsEvents.OnAnimationEnd += Shrugged;
            }
            else
                StartBehaviour();
        }

        protected override void StartBehaviour()
        {
            Interruptible = false;
            Started = true;


            girlMovementHandler.GoToDestination(interactionPosition.position, 0.08f, turnToPosition: transform, OnReachedEvent: OnReachedSword, OnFailedToMove: EndBehaviour);
            
        }

        private void OnReachedSword()
        {
            if (!hasStrength)
            {
                StruggleToPullSword();
            }
            else
            {
                PullSword();
            }
        }

        public void Shrugged()
        {
            girlAnimationsEvents.OnAnimationEnd -= Shrugged;
            EndBehaviour();
        }

        private void StruggleToPullSword()
        {
            foreach (FullBodyBipedEffector e in struggleEffectors)
            {
                girlInteractionSystem.StartInteraction(e, struggleInteraction, false);
            }
        }

        private void PullSword()
        {
            foreach (FullBodyBipedEffector e in pullSwordEffectors)
            {
                girlInteractionSystem.StartInteraction(e, pullSwordInteraction, false);
            }
        }

        public void SwordPulled(bool pulled)
        {
            if (pulled)
            {
                //girlIK.AssignItemToHand(gameObject.GetComponent<ItemStats>());
                rock.GetComponent<Rigidbody>().useGravity = true;
                magnetStone.GetComponent<Rigidbody>().isKinematic = false;
                girlInventory.AddSkill("Sword");
                girlAnimationsEvents.SheathSword();
                girlAnimationsEvents.OnAnimationEnd += SwordSheathed;

                onSwordPulled?.Invoke();
            }
            else
                EndBehaviour();
        }

        public void SwordSheathed()
        {
            girlAnimationsEvents.OnAnimationEnd -= SwordSheathed;
            girlAnimationsEvents.SwordSheathed();
            EndBehaviour();
        }


        protected override void EndBehaviour()
        {
            Ended = true;
            Started = false;
        }


        public override void Interrupt()
        {

        }
    }
}