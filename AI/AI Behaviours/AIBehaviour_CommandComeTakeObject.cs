﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Kandooz.KVR;
using RootMotion.FinalIK;

namespace Kandooz.Sausage
{
    public class AIBehaviour_CommandComeTakeObject : AIBehaviour
    {

        [SerializeField] private Transform orderComePosition;
        [SerializeField] private Transform player;
        [SerializeField] private Transform girlLeftHandPosition;
        [SerializeField] private Transform girlRightHandPosition;
        [SerializeField] private GameObject girlHandTrigger;
        [SerializeField] private MovementHandler movementHandler;
        [SerializeField] private float minStopDistance = 0.5f;

        [SerializeField] private GirlHandState girlHandState;

        [SerializeField] private GirlIK girlIK;
        [SerializeField] private float waitTime = 10;
        [SerializeField] private float waitExtendHandingDuration = 2.5f;

        private Transform girlHandPosition;
        private NPCLookAt looker;

        private GameObject objectToGive;
        private bool itemGiven, reached, handExtended, extendingHand, itemIn;

        float startMinDistance = 0;


        private void OnMouseDown()
        {
            FetchMe();
        }

        public void FetchMe()
        {
            GameObject.FindObjectOfType<AIHandler>().SetBehaviour(this);
        }

        public override void Init(AIHandler aIHandler)
        {
            Debug.Log("Init Come Called");
            base.Init(aIHandler);
            startMinDistance = 0;
            tries = 0;
            itemGiven = reached=  handExtended = extendingHand = itemIn = false;
            if (girlIK == null) girlIK = myHandler.GetComponent<GirlIK>();
            movementHandler = myHandler.GetComponent<MovementHandler>();
            if (girlIK != null) looker = girlIK.GetComponent<NPCLookAt>();
            if(player == null) player = Camera.main.transform;
            if (girlHandState == null) girlHandState = myHandler.GetComponent<GirlHandState>();

            if (girlIK == null ||
                orderComePosition == null ||
                movementHandler == null ||
                (girlLeftHandPosition == null && girlRightHandPosition == null) ||
                looker == null ||
                player == null ||
                girlHandState == null)
            {
                Debug.LogError("Missing Reference");
                EndBehaviour();
            }
            else
            {
                StartBehaviour();
            }

        }

        public override void Interrupt()
        {
            if (!Interruptible) return;

            //Stop Moving
            movementHandler.Stop();

            StopAllCoroutines();
            if (!itemGiven)
            {
                StopAllCoroutines();
                if (handExtended) girlIK.ReturnHand();
                EndBehaviour();
            }
        }
        int tries = 0;
        protected override void StartBehaviour()
        {

            Debug.Log("### COME ORDER STARTED");
            girlHandTrigger.SetActive(true);

            if (girlHandState.CheckHand(GirlHand.Right))
            {
                girlHandState.UnRegisterItem(GirlHand.Right);
            }
            bool moving = movementHandler.GoToDestination(orderComePosition.position, minStopDistance + startMinDistance, onReachCoroutine: StandAndWaitOrder(), turnToPosition: player);
            if(moving)
            {
                looker.FaceObject(player.gameObject);
                Interruptible = true;
                Started = true;
            }
            else
            {
                Debug.Log("Failed to Reach Player "+ tries);
                startMinDistance += 0.1f;
                tries++;

                if (tries > 50)
                {
                    Debug.Log("TOO MANY TRIES DYING");
                    EndBehaviour();

                }
                else StartBehaviour();


            }
        }

        

        protected override void EndBehaviour()
        {
            //looker.FaceObject(null);
            StopAllCoroutines();
            if(girlHandTrigger != null) girlHandTrigger.SetActive(false);
            if (looker != null) looker.FaceObject(null);
            Interruptible = false;
            Started = false;
            Ended = true;
         //   Debug.Log("Come behaviour ENDED");
        }

        private void Update()
        {
            //TESTING
            if (Input.GetKeyDown(KeyCode.C))
            {
                FetchMe();
            }

            if (!Started) return;


            if (reached && !itemGiven)
            {
                if (itemIn && handExtended)
                {
                    GiveItem();
                }
            }

            #region old stuff
            //if (GivenObject != null &&
            //    girlHandPosition != null &&
            //    handExtended == true &&
            //    itemGiven == false &&
            //    WaitingToTakeItem == true)
            //{
            //    if (CheckReachDistance(GivenObject.transform.position, girlHandPosition.position, 0.5f))
            //    {
            //        itemGiven = true;
            //        WaitingToTakeItem = false;
            //        Interruptible = false;
            //        StopAllCoroutines();

            //        //GirlItem item = givenObject.GetComponent<GirlItem>();
            //        //if (item != null) Destroy(item);

            //        //Most probable the item is a grabbable, so we force ungrab it first (before changing the parent)
            //        InteractableGrabbable itemGrabbable = GivenObject.GetComponent<InteractableGrabbable>();
            //        if (itemGrabbable != null) itemGrabbable.UnGrabMe();

            //        //In case the rb was not kinematic we make it kinematic again since the step above
            //        Rigidbody itemRB = GivenObject.GetComponent<Rigidbody>();
            //        if (itemRB != null) itemRB.isKinematic = true;

            //        // Now put item in her hand
            //        GivenObject.transform.parent = girlHandPosition;

            //        // Lean the movement of the object to the hand
            //        // when reached the girl returns her hand and stops facing you
            //        LeanTween.move(GivenObject, girlHandPosition, 0.5f).setOnComplete(() =>
            //        {
            //            GivenObject.transform.parent = girlHandPosition;
            //            handExtended = false;
            //            Debug.Log("Return Hand");
            //            girlIK.ReturnHand();
            //            /// girlIK.LookAt.FaceObject(null);
            //        });

            //        //Now Add the Behaviour on the item to the girl to be applied next should be VeryHighPriority or give it a high priority value
            //        //OLD
            //        ////AIBehaviour givenObjectBehaviour = GivenObject.GetComponent<AIBehaviour>();
            //        ////if (givenObjectBehaviour != null) myHandler.SetBehaviour(givenObjectBehaviour, 100);
            //        GirlItem givenObjectBehaviour = GivenObject.GetComponent<GirlItem>();
            //        if (givenObjectBehaviour != null)
            //        {
            //            Debug.Log("GirlItem Found:  Invoking");
            //            givenObjectBehaviour.OnItemGivenBehaviour?.Invoke();
            //        }
            //        Debug.Log("TakenItem Waiting 3 secs to end");
            //        StartCoroutine(WaitThenExcute(3, EndBehaviour));
            //    }
            //}
            #endregion
        }

        private IEnumerator StandAndWaitOrder()
        {

            yield return new WaitForSeconds(1);
            Debug.Log("Reached and Waiting Order");
            reached = true;

            yield return new WaitForSeconds(waitTime);
            Debug.Log("Duration Ended");
            if (!itemGiven)
            {
                StopAllCoroutines();
                if (handExtended)
                {
                    Debug.Log("returning hand");
                    girlIK.ReturnHand();
                    handExtended = false;
                }
                EndBehaviour();
            }
        }
        
        public void StartExtendHand(GameObject objectToGive, FullBodyBipedEffector hand = FullBodyBipedEffector.RightHand, string customHandTransform = "None")
        {
            if (itemGiven) return;

            itemIn = true;
            this.objectToGive = objectToGive;

            if (handExtended)
            {
                // GiveItem to her
                GiveItem();
                return;
            }

            if (!extendingHand)
            {
                switch (hand)
                {
                    case FullBodyBipedEffector.LeftHand:
                        girlHandPosition = girlLeftHandPosition;
                        break;
                    case FullBodyBipedEffector.RightHand:
                        girlHandPosition = girlRightHandPosition;
                        break;
                }
                if (girlHandPosition == null)
                {
                    Debug.Log("No Hand To Give item To");
                    return;
                }
                if(customHandTransform != "None" && !String.IsNullOrEmpty(customHandTransform))
                {
                   Transform customTransform = girlHandPosition.Find(customHandTransform);
                   if (customTransform != null) girlHandPosition = customTransform;
                    Debug.Log("customTransform: " + customTransform);
                    Debug.Log(girlHandPosition);
                }

                Interruptible = false;
                // On Finish Extending hand
                if (girlIK.ExtendHand(hand)) // Extend her hand      
                {
                    extendingHand = true;
                    girlIK.HandExtended += OnHandExtended;
                   // Invoke("OnHandExtended", 0.5f);
                }

            }

            
        }

        internal void ItemOut()
        {
            itemIn = false;
            //this.objectToGive = null;
        }

        public void OnHandExtended()
        {
            Debug.Log("OnHandExtended");
            handExtended = true;
            extendingHand = false;

            girlIK.HandExtended -= OnHandExtended;

            if (itemIn) GiveItem();
            else StartCoroutine(HandExtendingDuration());
        }

        private void GiveItem()
        {
            if (itemGiven) return;
            if (objectToGive == null) return;
            itemGiven = true;

            //looker.FaceObject(objectToGive);

            //Most probable the item is a grabbable, so we force ungrab it first (before changing the parent)
            InteractableGrabbable itemGrabbable = objectToGive.GetComponent<InteractableGrabbable>();
            if (itemGrabbable != null) itemGrabbable.UnGrabMe();

            //In case the rb was not kinematic we make it kinematic again since the step above
            Rigidbody itemRB = objectToGive.GetComponent<Rigidbody>();
            if (itemRB != null) itemRB.isKinematic = true;

            // Now put item in her hand
            //objectToGive.transform.parent = girlHandPosition;

            // Lean the movement of the object to the hand
            // when reached the girl returns her hand and stops facing you
            LeanTween.move(objectToGive, girlHandPosition, 0.5f).setOnComplete(() =>
            {
                objectToGive.transform.parent = girlHandPosition;
            //    Debug.Log("Return Hand");
                girlIK.ReturnHand();
            });
            LeanTween.rotate(objectToGive, Vector3.zero, 0.5f);

            //Now Add the Behaviour on the item to the girl to be applied next should be VeryHighPriority or give it a high priority value
            GirlItem givenObjectBehaviour = objectToGive.GetComponent<GirlItem>();
            if (givenObjectBehaviour != null)
            {
              //  Debug.Log("GirlItem Found:  Invoking");
                givenObjectBehaviour.OnItemGivenBehaviour?.Invoke();
                //looker.FaceObject(null);
            }
        //    Debug.Log("TakenItem Waiting 2 secs to end");
            Invoke("EndBehaviour", 2);
        }

        private IEnumerator HandExtendingDuration()
        {
            yield return new WaitForSeconds(waitExtendHandingDuration);
            if (!itemGiven)
            {
                girlIK.ReturnHand();
                handExtended = false;
            }
        }
        

    }
}

