﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kandooz.Sausage
{
    public class AIBehaviour_ArtifactActivation : AIBehaviour
    {
        public Transform interactionPosition;
        public Transform ball;
        public List<Transform> ballPieces;
        public Transform item;
        public Transform itemDropPosition;

        private InventoryManager girlInventory;
        private MovementHandler girlMovement;
        private GirlAnimationsEvents girlAnimationsEvents;
        private NPCLookAt girlLookAt;
        private GirlHandState handstate;

        private bool used;
        private bool hasSkill;


        private void OnMouseDown()
        {
            FetchMe();
        }

        public void FetchMe()
        {
            if (used) //already activated
            {
                Debug.Log("Already Activated");
                return;
            }

            FindObjectOfType<AIHandler>().SetBehaviour(this);
        }

        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);

            Interruptible = false;

            girlInventory = myHandler.GetComponent<InventoryManager>();
            girlMovement = myHandler.GetComponent<MovementHandler>();
            girlAnimationsEvents = myHandler.GetComponent<GirlAnimationsEvents>();
            girlLookAt = myHandler.GetComponent<NPCLookAt>();
            handstate = myHandler.GetComponent<GirlHandState>();

            if (girlInventory.SearchSkill("Light"))
                hasSkill = true;

            if (handstate.CheckHand(GirlHand.Right))
            {
                girlAnimationsEvents.Shrug();
                girlAnimationsEvents.OnAnimationEnd += Shrugged;
            }
            else
                StartBehaviour();
        }

        protected override void StartBehaviour()
        {
            Interruptible = false;
            Started = true;

            girlMovement.GoToDestination(interactionPosition.position, 1f, OnReachedEvent: OnReachArtifact, turnToPosition: transform, OnFailedToMove: EndBehaviour);
        }

        private void OnReachArtifact()
        {
            if (hasSkill)
            {
                //activate
                Debug.Log("Can Activate");
                girlLookAt.FaceObject(ball.gameObject);
                girlLookAt.PointHand(ball.gameObject, AvatarIKGoal.RightHand);
                girlLookAt.PointHand(ball.gameObject, AvatarIKGoal.LeftHand);
                girlAnimationsEvents.Cast(true);
                Invoke("BallRise", 3);
            }
            else
            {
                //shrug
                Debug.Log("No Skill, shrug");
                girlAnimationsEvents.Shrug();
                girlAnimationsEvents.OnAnimationEnd += Shrugged;
            }
        }

        public void Shrugged()
        {
            girlAnimationsEvents.OnAnimationEnd -= Shrugged;
            EndBehaviour();
        }


        private void BallRise()
        {
            LeanTween.moveY(ball.gameObject, ball.transform.position.y + 0.8f, 3).setOnComplete(()=> {
                BallEffect();
            });
        }

        private void BallEffect()
        {
            for(int i=0; i< ballPieces.Count; i++)
            {
                if(ballPieces[i].GetComponent<Ball>())
                {
                    LeanTween.move(ballPieces[i].gameObject, ballPieces[i].GetComponent<Ball>().newPos.position, 4);
                }
            }
            Invoke("ItemDrop", 5);
        }


        private void ItemDrop()
        {
            LeanTween.move(item.gameObject, itemDropPosition, 3).setOnComplete(() => {

                for (int i = 0; i < ballPieces.Count; i++)
                {
                    ballPieces[i].gameObject.AddComponent<Rigidbody>();
                    girlLookAt.AvoidFacing();
                    girlLookAt.AvoidPointing();
                    girlAnimationsEvents.Cast(false);
                    Invoke("EndBehaviour", 2);
                }
                used = true;
            });
        }

        protected override void EndBehaviour()
        {
            Ended = true;
            Started = false;
        }


        public override void Interrupt()
        {
        }

        void Start()
        {
        }
    }
}
