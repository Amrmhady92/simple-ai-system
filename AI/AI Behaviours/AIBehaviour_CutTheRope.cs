﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kandooz.Sausage
{
    public class AIBehaviour_CutTheRope : AIBehaviour
    {
        public Transform interactionPosition;
        public GameObject item;

        private InventoryManager girlInventory;
        private MovementHandler girlMovement;
        private GirlAnimationsEvents girlAnimationsEvents;
        private GirlHandState handstate;

        private bool used;
        private bool hasSword;


        private void OnMouseDown()
        {
            FetchMe();
        }

        public void FetchMe()
        {
            if (used) //already unlocked
            {
                Debug.Log("Already used");
                return;
            }

            FindObjectOfType<AIHandler>().SetBehaviour(this);
        }

        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);

            Interruptible = false;

            girlInventory = myHandler.GetComponent<InventoryManager>();
            girlMovement = myHandler.GetComponent<MovementHandler>();
            girlAnimationsEvents = myHandler.GetComponent<GirlAnimationsEvents>();
            handstate = myHandler.GetComponent<GirlHandState>();

            if (girlInventory.SearchSkill("Sword"))
                hasSword = true;

            if (handstate.CheckHand(GirlHand.Right))
            {
                girlAnimationsEvents.Shrug();
                girlAnimationsEvents.OnAnimationEnd += Shrugged;
            }
            else
                StartBehaviour();
        }


        protected override void StartBehaviour()
        {
            Interruptible = false;
            Started = true;

            girlMovement.GoToDestination(interactionPosition.position, 1f, turnToPosition: transform, OnReachedEvent: OnReachRope, OnFailedToMove: EndBehaviour);
        }

        private void OnReachRope()
        {
            if (hasSword)
            {
                //cut with sword
                Debug.Log("Unsheath sword");
                Debug.Log("has sword, cut the rope");
                girlAnimationsEvents.DrawSword();
                girlAnimationsEvents.OnAnimationEnd += SwordDrawed;
            }
            else
            {
                //shrug
                Debug.Log("No Sword, shrug");
                girlAnimationsEvents.Shrug();
                girlAnimationsEvents.OnAnimationEnd += Shrugged;
            }
        }

        public void Shrugged()
        {
            girlAnimationsEvents.OnAnimationEnd -= Shrugged;
            EndBehaviour();
        }

        public void SwordDrawed()
        {
            girlAnimationsEvents.OnAnimationEnd -= SwordDrawed;
            girlAnimationsEvents.UnlockCabinet();
            girlAnimationsEvents.OnAnimationEnd += OnRopeCut;
        }

        private void OnRopeCut()
        {
            used = true;
            item.GetComponent<Rigidbody>().isKinematic = false; //COMMENT this line and call the cutting function
            girlAnimationsEvents.OnAnimationEnd -= OnRopeCut;
            girlAnimationsEvents.SheathSword();
            girlAnimationsEvents.OnAnimationEnd += SwordSheathed;
        }

        private void SwordSheathed()
        {
            girlAnimationsEvents.OnAnimationEnd -= SwordSheathed;
            girlAnimationsEvents.SwordSheathed();
            EndBehaviour();
        }


        protected override void EndBehaviour()
        {
            Ended = true;
            Started = false;
        }


        public override void Interrupt()
        {
        }

        void Start()
        {
        }
    }
}