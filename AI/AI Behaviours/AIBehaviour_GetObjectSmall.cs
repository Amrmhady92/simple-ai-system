﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine;
using Kandooz.KVR;
using System;
using RootMotion.FinalIK;

namespace Kandooz.Sausage
{

    public class AIBehaviour_GetObjectSmall : AIBehaviour
    {
        public InteractionObject telekinesisInteraction;

        public Transform pickingUpPositionTransform;
        public Transform dropOffTarget;
        public Transform player;
        public GirlIK pickUpGirl;
        public float minPickUpDistance = 1f;
        public float minStoppingDistance = 0.5f;
        public float minStoppingDistanceAtPlayer = 0.5f;
        public float handExtendingDuration = 10;
        public bool canBeFetched = true;
        public bool keepItem = false;
        public float dropAfter = 30f;
        public float teleknisisRange = 5;
        [HideInInspector]public bool enableDropAfter = true;
        public UnityEvent onItemPickedUp;
        public UnityEvent onItemDropped;
        public UnityEvent onItemGiven;



        private Transform originalParent;
        private NavMeshAgent agent;
        private Rigidbody rb;
        private GirlHandState handstate;
        private MovementHandler movementHandler;
        private InventoryManager girlInventory;
        private GirlAnimationsEvents girlAnimationsEvents;
        private NPCLookAt girlLookAt;

        private float agentsOriginalStoppingDist;
        private bool pickedUp = false;
        private bool itemGivenToPlayer = false;
        private bool firstAttempt = true;



        private void OnMouseDown()
        {
            FetchMe();
        }

        public void FetchMe()
        {

            GameObject.FindObjectOfType<AIHandler>().SetBehaviour(this);
        }


        public override void Init(AIHandler aIHandler)
        {
            if (pickedUp)
            {

            }
            Debug.Log("Init Pick Up Called");
            base.Init(aIHandler);
            agent = myHandler.GetComponent<NavMeshAgent>();
            girlInventory = myHandler.GetComponent<InventoryManager>();
            girlAnimationsEvents = myHandler.GetComponent<GirlAnimationsEvents>();
            girlLookAt = myHandler.GetComponent<NPCLookAt>();
            handstate = myHandler.GetComponent<GirlHandState>();
            movementHandler = myHandler.GetComponent<MovementHandler>();
            enableDropAfter = true;
            firstAttempt = true;
            StopAllCoroutines();

            //Check Hands
            bool handsFull = false;
            if (handstate == null) handsFull = handstate.CheckHand(GirlHand.Right);

            // Get the Girl IK
            if (pickUpGirl == null) pickUpGirl = myHandler.GetComponent<GirlIK>();

            if (pickUpGirl == null ||
                (dropOffTarget == null && !keepItem) ||
                !canBeFetched ||
                handsFull ||
                movementHandler == null)
            {
                Debug.LogError("GirlIK = " + pickUpGirl +
                    "\nDropOffTarget = " + dropOffTarget + 
                    "\nCan be Fetched = " + canBeFetched + 
                    "\nHandsFull = " + handsFull);

                if(!canBeFetched)
                {
                    girlAnimationsEvents.Shrug();
                    girlAnimationsEvents.OnAnimationEnd += EndBehaviour;
                }
                else
                    EndBehaviour();
            }
            else
            {
                StartBehaviour();
            }

        }

        public void CanBeFetched(bool can)
        {
            canBeFetched = can;
        }

        public override void Interrupt()
        {
            if (!Interruptible) return;

            //Stop Moving

            movementHandler.Stop();


            StopAllCoroutines();

            if (pickedUp)
            {
                StartCoroutine(DropDuration());
            }
            else
            {
                StartCoroutine(Wait(1, EndBehaviour));
            }


        }

        protected override void StartBehaviour()
        {
            Interruptible = true;
            Started = true;

            Debug.Log("GetObjectSmall: Started");
            pickedUp = false;
            itemGivenToPlayer = false;

            originalParent = this.transform.parent;
            rb = this.GetComponent<Rigidbody>();

            if (pickingUpPositionTransform == null) pickingUpPositionTransform = this.transform;
           bool moving = movementHandler.GoToDestination(pickingUpPositionTransform.position, minStoppingDistance, OnReachedPickUp, OnFailedToMove: FailedToArriveObject, turnToPosition: this.transform , targetName:gameObject.name);
            if(moving)
            {
                girlLookAt.FaceObject(gameObject, weight: 0.5f);
            }
        }

        public void FailedToArriveObject()
        {
            Debug.Log("Failed to Arrive");
            if(girlInventory.SearchSkill("Tele") && firstAttempt)
            {
                Debug.Log("Attempting to do Teleknisis");

                firstAttempt = false;
                movementHandler.GoToDestination(transform.position, teleknisisRange, OnReachedEvent: RaiseHandAndBeginTeleknisis, OnFailedToMove: FailedToArriveObject);
            }
            else
            {
                //movementHandler.GoToDestination(interactionPosition.position, 4, OnReachedEvent: RaiseHand);
                //Invoke("RaiseHand", 3);
                if (!girlInventory.SearchSkill("Tele")) Debug.Log("No Teleknisis, nothing happens (should shrug)");
                else Debug.Log("Couldnt Reach Teleknisis Point");

                //if(movementHandler.GoToDestination(myHandler.transform.position, 0.1f, OnReachedEvent: girlAnimations.Shrug, turnToPosition: this.transform /*The Player*/, OnFailedToMove: girlAnimations.Shrug))
                //{
                //    Debug.Log("Success Turn");
                //}
                girlAnimationsEvents.Shrug();
                girlAnimationsEvents.OnAnimationEnd += Shrugged;
            }
            
        }

        private void RaiseHandAndBeginTeleknisis()
        {
            girlLookAt.FaceObject(gameObject);
            girlLookAt.PointHand(gameObject, AvatarIKGoal.RightHand);
            Invoke("FloatObject", 2);
        }

        public void FloatObject()
        {
            pickUpGirl.FloatItemToGirl(gameObject, telekinesisInteraction);
            pickUpGirl.PickedUpItem += AfterItemPicked;
        }


        public void Shrugged()
        {
            girlAnimationsEvents.OnAnimationEnd -= Shrugged;
            EndBehaviour();
        }

        protected override void EndBehaviour()
        {
            Debug.Log("Get Object Ended");
            Started = false;
            Ended = true;
        }

        private void Update()
        {

            if (Input.GetKeyDown(KeyCode.S))
            {
                FetchMe();
            }

            if (!started) return;

           
        }

        private void OnReachedPickUp()
        {
            movementHandler.Stop();
            StartCoroutine(PickUpDuration());
        }

        private void OnReachedDropPosition()
        {


            movementHandler.Stop();
            StartCoroutine(DropDuration());
        }

        private bool CheckReachDistance(Vector3 target1, Vector3 target2, float minDist)
        {
            float dist = Vector3.Distance(target1, target2);
            return (dist <= minDist);
        }

        private IEnumerator PickUpDuration()
        {
            Debug.Log("Pick up Started");
            yield return new WaitForSeconds(1);

            if (pickUpGirl.InitiatePickUp(this.gameObject, FullBodyBipedEffector.RightHand))
            {
                Debug.Log("Initiate PickUp Item Pick Up Started");
                girlLookAt.FaceObject(null);

                pickUpGirl.PickedUpItem += AfterItemPicked;
            }
            else
            {
                Debug.Log("Initiate PickUp Item Failed");
                //movementHandler.GoToDestination(this.transform.position, minStoppingDistance, OnReachedDropPosition);
                //EndBehaviour();
                girlAnimationsEvents.Shrug();
                girlAnimationsEvents.OnAnimationEnd += Shrugged;
            }

        }

        private IEnumerator DropDuration()
        {
            Interruptible = false;
            yield return new WaitForSeconds(1);

            //pickUpGirl.ExtendHand();
            pickUpGirl.GiveItem(); // Extend grabbing hand to player for him to take the object
            InteractableGrabbable grabbable = this.GetComponent<InteractableGrabbable>();
            if (grabbable != null)
            {
                grabbable.OnGrab.AddListener(WaitPlayerGrabbed); // Add listener to check if the player picked up or not
            }



            yield return new WaitForSeconds(handExtendingDuration); // Stay extend for a while
            if (!itemGivenToPlayer) // If he hasnt picked it yet
            {
                girlLookAt.FaceObject(null);

                pickUpGirl.ReturnHand();
               
                Debug.Log("DROP FROM WAIT GIVE");
                DropMe();
                grabbable.OnGrab.RemoveListener(WaitPlayerGrabbed);
                Interruptible = true;
                agent.stoppingDistance = agentsOriginalStoppingDist;
                
                EndBehaviour();
            }
        }


        private void AfterItemPicked()
        {
            Debug.Log("After Item Picked Up");

            if (keepItem)
            {
                StartCoroutine(Wait(dropAfter, () =>
                {
                    Debug.Log("DROP FROM WAIT KEEP");
                    if (enableDropAfter) DropMe();
                }));
                EndBehaviour(); 
            }
            else
            {
                Transform turnTo = player;
                if (turnTo == null) turnTo = dropOffTarget;
                movementHandler.GoToDestination(dropOffTarget.position, minStoppingDistanceAtPlayer, OnReachedDropPosition, turnToPosition:player);
                girlLookAt.FaceObject(player.gameObject);
            }

            onItemPickedUp?.Invoke();

            pickedUp = true;

            handstate.RegisterItem(this.gameObject, GirlHand.Right);

            pickUpGirl.PickedUpItem -= AfterItemPicked;
            Debug.Log("AfterItemPicked");
        }

        public void WaitPlayerGrabbed()
        {
            pickUpGirl.ReturnHand();
            itemGivenToPlayer = true;
            this.GetComponent<InteractableGrabbable>().OnGrab.RemoveListener(WaitPlayerGrabbed);
            onItemGiven?.Invoke();
            handstate.UnRegisterItem(GirlHand.Right);

            Interruptible = true;
            //agent.stoppingDistance = agentsOriginalStoppingDist;
            EndBehaviour();
        }

        private IEnumerator Wait(float waitTime, Action callBack)
        {
            yield return new WaitForSeconds(waitTime);
            callBack.Invoke();
        }

        public void DropMe()
        {
            
            pickUpGirl.ReturnHand();
            StartCoroutine(wait());
        }

        IEnumerator wait()
        {
            yield return new WaitForSeconds(1f);

            girlAnimationsEvents.Throw();
            girlAnimationsEvents.OnAnimationEnd += Dropped;
        }

        private void Dropped()
        {
            this.transform.parent = originalParent;
            if (rb != null)
            {
                rb.isKinematic = false;
                rb.useGravity = true;

                Debug.Log("DROP ME");
            }
            handstate.UnRegisterItem(GirlHand.Right);
            pickedUp = false;
            //pickUpGirl.ReturnHand();
            onItemDropped?.Invoke();
        }
    }
}
