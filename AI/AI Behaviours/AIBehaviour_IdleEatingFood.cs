﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;


namespace Kandooz.Sausage
{
    public class AIBehaviour_IdleEatingFood : AIBehaviour
    {
        public List<Transform> foodPositions;
        public InteractionObject telekinesisInteraction;

        public float teleknisisRange = 5;

        private MovementHandler girlMovementHandler;
        private GirlAnimationsEvents girlAnimations;
        private GirlHandState handstate;
        private GirlIK girlIK;
        private InventoryManager girlInventory;
        private NPCLookAt girlLookAt;

        private GameObject currentFood;
        private Transform originalParent;
        private bool firstAttempt = true;
        private bool eating;
        

        public override void Init(AIHandler aIHandler)
        {
            base.Init(aIHandler);

            if (foodPositions.Count != 0)
            {
                StartBehaviour();
            }
            else
            {
                Debug.Log("No Food");
                EndBehaviour();
            }
        }

        protected override void StartBehaviour()
        {
            girlMovementHandler = myHandler.GetComponent<MovementHandler>();
            girlAnimations = myHandler.GetComponent<GirlAnimationsEvents>();
            handstate = myHandler.GetComponent<GirlHandState>();
            girlIK = myHandler.GetComponent<GirlIK>();
            girlInventory = myHandler.GetComponent<InventoryManager>();
            girlLookAt = myHandler.GetComponent<NPCLookAt>();

            firstAttempt = true;

            if (!handstate.CheckHand(GirlHand.Right))
            {
                Started = true;
                Interruptible = true;
                FindFood();
            }
            else
                EndBehaviour();
        }

        private void FindFood()
        {
            int rndNum = Random.Range(0, foodPositions.Count);
            currentFood = foodPositions[rndNum].gameObject;
            originalParent = foodPositions[rndNum].transform.parent;
            telekinesisInteraction = currentFood.GetComponentInChildren<InteractionObject>();
            girlMovementHandler.GoToDestination(foodPositions[rndNum].position, 0.5f, turnToPosition: foodPositions[rndNum], OnReachedEvent: PickUp, OnFailedToMove: FailedToArriveObject);
        }

        private void FailedToArriveObject()
        {
            if (girlInventory.SearchSkill("Tele") && firstAttempt)
            {
                firstAttempt = false;
                girlMovementHandler.GoToDestination(currentFood.transform.position, teleknisisRange, turnToPosition: currentFood.transform, OnReachedEvent: RaiseHandAndBeginTeleknisis, OnFailedToMove: FailedToArriveObject);
            }
            else
            {
                Debug.Log("No Telekinesis");
                EndBehaviour();
            }
        }

        private void RaiseHandAndBeginTeleknisis()
        {
            eating = true;
            girlLookAt.FaceObject(currentFood);
            girlLookAt.PointHand(currentFood, AvatarIKGoal.RightHand);
            Invoke("FloatObject", 2);
        }

        public void FloatObject()
        {
            girlIK.FloatItemToGirl(currentFood, telekinesisInteraction);
            girlIK.PickedUpItem += AfterItemPicked;
        }

        private void PickUp()
        {
            Debug.Log("Reached Food");
            eating = true;
            StartCoroutine(PickUpDuration());
        }

        private IEnumerator PickUpDuration()
        {
            yield return new WaitForSeconds(1);
            Debug.Log("before girl ik initiate pick up");
            if (girlIK.InitiatePickUp(currentFood, FullBodyBipedEffector.RightHand))
            {
                girlIK.PickedUpItem += AfterItemPicked;
            }
            else
            {
                Debug.Log("PickUp Item Failed");
                EndBehaviour();
            }
        }

        private void AfterItemPicked()
        {
            Debug.Log("after item pickedUp event called");
            girlIK.PickedUpItem -= AfterItemPicked;

            girlAnimations.Eat();
            girlAnimations.OnAnimationEnd += FinishedEating;
        }

        private void FinishedEating()
        {
            girlAnimations.OnAnimationEnd -= FinishedEating;
            currentFood.SetActive(false);
            currentFood.transform.parent = originalParent;
            foodPositions.Remove(currentFood.transform);
            girlIK.ReturnHand();
            currentFood = null;
            EndBehaviour();
        }

        protected override void EndBehaviour()
        {
            eating = false;
            Ended = true;
            Started = false;
        }


        public override void Interrupt()
        {
            if (!eating)
                EndBehaviour();
        }

        void Start()
        {
        }
    }
}