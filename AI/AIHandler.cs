﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine.Events;
using UnityEngine;
using System;

namespace Kandooz.Sausage
{
    public class AIHandler : MonoBehaviour
    {
        public bool usePriority = true;
        public bool queueLimited = false;
        public int queueMaxLimit = 3;
        public List<AIBehaviour> idleBehaviours;
        [SerializeField] private List<AIBehaviour> usedIdleBehaviours;

        [Space(10)]
        [SerializeField] private AIBehaviour currentAIBehaviour;
       // private AIBehaviour previousBehavior;
        private List<AIBehaviour> behavioursQueue;
        private AIBehaviour interruptingBehaviour;

        //private AIBehaviour testBehaviour1;
        //private AIBehaviour testBehaviour2;

        public AIBehaviour CurrentAIBehaviour
        {
            get { return currentAIBehaviour; }

            protected set { currentAIBehaviour = value; }
        }

        private void Start()
        {
            behavioursQueue = new List<AIBehaviour>();
            usedIdleBehaviours = new List<AIBehaviour>();
            //testBehaviour1 = this.gameObject.AddComponent<AIBehaviour_IdleWalkingAround>();
            //testBehaviour2 = this.gameObject.AddComponent<AIBehaviour_IdleStanding>();
        }


        private void Update()
        {
            CheckCurrentBehaviourStatus();

            //TESTING

            //if (Input.GetKeyDown(KeyCode.Alpha1))
            //{
            //    SetBehaviour(testBehaviour1, 2);
            //}

            //if (Input.GetKeyDown(KeyCode.Alpha2))
            //{
            //    SetBehaviour(testBehaviour2);
            //}
        }

        /// <summary>
        /// Update Function , to check on the current Behaviour if ended or not,
        /// when the behaviour ends, it starts the next behaviour from the queue 
        /// if existed else will use an idle behaviour from the list.
        /// Uncheck usePriority to use the first Behaviour given in order.
        /// </summary>
        private void CheckCurrentBehaviourStatus()
        {
            if (CurrentAIBehaviour != null)
            {
                if (!CurrentAIBehaviour.Ended) return;

                CurrentAIBehaviour = null;

                if (interruptingBehaviour != null)
                {
                    CurrentAIBehaviour = interruptingBehaviour;
                    CurrentAIBehaviour.Init(this);
                    interruptingBehaviour = null;
                }
                else if (behavioursQueue.Count > 0)
                {
                    if (usePriority)
                    {
                        AIBehaviour highestPrioirtyBehaviour;
                        highestPrioirtyBehaviour = behavioursQueue[0];
                        for (int i = 0; i < behavioursQueue.Count; i++)
                        {
                            if (behavioursQueue[i].IsVeryHighPriority)
                            {
                                highestPrioirtyBehaviour = behavioursQueue[i];
                                break;
                            }
                            else if (highestPrioirtyBehaviour.Priority < behavioursQueue[i].Priority)
                            {
                                highestPrioirtyBehaviour = behavioursQueue[i];
                            }
                        }
                        CurrentAIBehaviour = highestPrioirtyBehaviour;
                        CurrentAIBehaviour.Init(this);
                        behavioursQueue.RemoveAt(behavioursQueue.IndexOf(highestPrioirtyBehaviour));
                    }
                    else
                    {
                        CurrentAIBehaviour = behavioursQueue[0];
                        CurrentAIBehaviour.Init(this);
                        behavioursQueue.RemoveAt(0);
                    }

                }
            }
            else // Idle Behaviours
            {
                AIBehaviour behaviour = null;

                //if (idleBehaviours.Count > 0)
                //{
                //    //behaviour = idleBehaviours.GetRandomValue();
                //    //if (behaviour != null)
                //    //{
                //    //    idleBehaviours.Remove(behaviour);
                //    //    usedIdleBehaviours.Add(behaviour);
                //    //}
                //}
                //else
                if(idleBehaviours.Count == 0)
                {
                    if(usedIdleBehaviours.Count > 0)
                    {
                        idleBehaviours = new List<AIBehaviour>();
                        for (int i = 0; i < usedIdleBehaviours.Count; i++)
                        {
                            idleBehaviours.Add(usedIdleBehaviours[i]);
                        }
                        usedIdleBehaviours = new List<AIBehaviour>();
                    }
                    else
                    {
                        Debug.Log("Added New Idle");

                        behaviour = this.gameObject.AddComponent<AIBehaviour_IdleStanding>();
                        idleBehaviours.Add(behaviour);
                    }
                }

                behaviour = idleBehaviours.GetRandomValue();
                if (behaviour != null)
                {
                    idleBehaviours.Remove(behaviour);
                    usedIdleBehaviours.Add(behaviour);
                }
                else//if (behaviour == null)
                {
                    if (idleBehaviours.Count == 0 && usedIdleBehaviours.Count == 0)
                    {
                        Debug.Log("Both idle and Used behaviour queues are empty");
                    }
                    else
                    {
                        Debug.Log("idleBehaviours.Count : " + idleBehaviours.Count +
                            "\nusedIdleBehaviours.Count: " + usedIdleBehaviours.Count +
                            "\nbehaviour = null");
                    }
                    behaviour = this.gameObject.AddComponent<AIBehaviour_IdleStanding>();
                    idleBehaviours.Add(behaviour);

                }

                SetBehaviour(behaviour);

                //if (behaviour != previousBehavior)
                //{
                //    SetBehaviour(behaviour);
                //    previousBehavior = behaviour;
                //}
                //else
                //{
                //    Debug.Log("SAME BEHAVIOR");
                //}
            }
        }

        #region Public Methods
        /// <summary>
        /// Sets a behaviour , Interrupts if the behaviour is interruptible 
        /// if not, the behaviour is put in the queue.
        /// </summary>
        /// <param name="behaviour"></param>
        /// <param name="priority"></param>
        public void SetBehaviour(AIBehaviour behaviour, int priority = 0)
        {
            //string behaviourName = behaviour == null ? "null" : behaviour.GetType().Name;
            //Debug.Log("Setting Behaviour: " + behaviourName);

            //Debug.Log("Queue Already Has Behaviour: "+ behaviour +" is :"+ behavioursQueue.Contains(behaviour));
            //Debug.Log("Interrupting Behaviour Already Has Behaviour: "+ behaviour +" is :"+ interruptingBehaviour == behaviour);
            if (behaviour.Started || behavioursQueue.Contains(behaviour) || (interruptingBehaviour != null && interruptingBehaviour == behaviour)) return; // If the behaviour is already in use, or in the queue dont do anything.

            if (CurrentAIBehaviour == null) // If there is no Behaviour at the moment
            {
                CurrentAIBehaviour = behaviour;
                CurrentAIBehaviour.Init(this);
            }
            else
            {
                if (CurrentAIBehaviour.Interruptible)
                {
                    CurrentAIBehaviour.Interrupt();
                    interruptingBehaviour = behaviour;
                }
                else if (behaviour.IsVeryHighPriority && interruptingBehaviour != null)
                {
                    interruptingBehaviour = behaviour;
                }
                else if (!queueLimited || (behavioursQueue.Count < queueMaxLimit && queueLimited) )
                {
                    behaviour.Priority = priority;
                    behavioursQueue.Add(behaviour);
                }
            }
        }
        #endregion

    }
}